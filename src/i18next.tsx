import i18next from "i18next";
import { initReactI18next } from "react-i18next";
import enTranslation from "./locales/en/name_space.json";
import esTranslation from "./locales/es/name_space.json";

i18next.use(initReactI18next)
.init({
  interpolation: { escapeValue: false }, // React already does escaping
  lng: "en", // Default language
  resources: {
    en: {
      translation: enTranslation,
    },
    es: {
      translation: esTranslation,
    },
  },
});

export default i18next;