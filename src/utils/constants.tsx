import {
  FaGithub,
  FaGitlab,
  FaLinkedin,
  FaCube,
  FaGoogle
} from "react-icons/fa";

import { SiCredly } from "react-icons/si";

export const url = {
  github: "https://github.com/mateolopezg",
  gitlab: "https://gitlab.com/users/jilguerillo",
  hackerthebox: "https://app.hackthebox.com/profile/650476",
  linkedin: "https://www.linkedin.com/in/mateo-elias-lopez-gomez",
  googlecloudskills: "https://partner.cloudskillsboost.google/public_profiles/770136a1-0229-4615-a136-ae4722aca34c",
  credly: "https://www.credly.com/users/mateo-lopez.d8f6ce1d/badges"
};

export const cardDescriptions = [
  
  {
    title: "",
    description: "",
    url: url.linkedin,
  },
  {
    title: "",
    description: "",
    url: url.gitlab,
  },
  {
    title: "",
    description:
      "",
    url: url.credly,
  },  
  {
    title: "",
    description:
      "",
    url: url.googlecloudskills,
  },
  {
    title: "",
    description: "",
    url: url.hackerthebox,
  },
  {
    title: "",
    description:
      "",
    url: url.github,
  },
];

export const linkItems = [
  {
    icon: FaLinkedin,
    link_: url.linkedin,
  },
  {
    icon: FaGitlab,
    link_: url.gitlab,
  },
  {
    icon: SiCredly,
    link_: url.credly,
  },
  {
    icon: FaGoogle,
    link_: url.googlecloudskills,
  },
  {
    icon: FaCube,
    link_: url.hackerthebox,
  },
  {
    icon: FaGithub,
    link_: url.github,
  },
];
