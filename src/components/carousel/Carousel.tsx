import "./Carousel.css"
import CardDescription from "../card-description/CardDescription";
import LinkItem from "../links/LinkItem";
import { cardDescriptions, linkItems, url } from "../../utils/constants";
import { useTranslation } from "react-i18next";


const Carousel = () => {

  const { t } = useTranslation();

  let title_: string;
  let description_: string;

  function validDesc(matching?: string): string {
    switch (matching) {
      case url.linkedin:
        return t("description.linkedin");
      case url.gitlab:
        return t("description.gitlab");
      case url.googlecloudskills:
        return t("description.googlecloudskills");
      case url.hackerthebox:
        return t("description.hackthebox");
      case url.github:
        return t("description.github");
      case url.credly:
          return t("description.credly");
      default:
        return "";
    }
  }

  function validTitle(matching?: string): string {
    switch (matching) {
      case url.linkedin:
        return t("title.linkedin");
      case url.gitlab:
        return t("title.gitlab");
      case url.googlecloudskills:
        return t("title.googlecloudskills");
      case url.hackerthebox:
        return t("title.hackthebox");
      case url.github:
        return t("title.github");
      case url.credly:
        return t("title.credly");        
      default:
        return "";
    }
  }

  return (
    <>
      {linkItems.map((linkItem) => {
        const { icon, link_ } = linkItem;
        const matching = cardDescriptions.find(
          (desc) => desc.url === link_
        );

        const readyUrl = matching?.url

        description_ = validDesc(readyUrl)
        title_ = validTitle(readyUrl)

        return (
          <div key={link_} className="container-item font-mono">
            <LinkItem icon={icon} link={link_} />
            <CardDescription
              title={title_}
              description={description_}
              url={link_}
            />
          </div>
        );
      })}
    </>
  );
};



export default Carousel;
