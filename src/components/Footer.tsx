import React from "react";

const Footer = () => {
  return (
    <footer className="footer footer-center p-4 bg-base-300 text-base-content m-0">
        <div>
          <p>Copyright © 2025 - All right reserved by Mateoti Industries Ltd</p>
        </div>
      </footer>
  );
};

export default Footer;
