import { useTranslation } from "react-i18next";

const Narvar = () => {
  const { t } = useTranslation();
  return (
    <div className="navbar bg-teal-500 rounded-box flex-wrap m-1">
        <a
          className="btn btn-ghost normal-case font-mono text-lg text-white flex-auto m-2"
          href="https://linkcitos.vercel.app/"
        >
          {t("title.narvar")}
        </a>
      </div>
  );
};

export default Narvar;
