import { useEffect, useRef, useState } from "react";
import "./Background.css"

interface Sphere {
  x: number;
  y: number;
  vx: number;
  vy: number;
  color: string;
}

const Background = () => {
  const [spheres, setSpheres] = useState<Sphere[]>([]);
  const containerRef = useRef<HTMLDivElement>(null);
  const requestRef = useRef<number | null>(null);

  const getRandomColor = () => {
    const letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  useEffect(() => {
    const screenWidth = window.innerWidth;
    const screenHeight = window.innerHeight;

    const initialSpheres: Sphere[] = Array.from({ length: 20 }).map(() => ({
      x: Math.random() * (screenWidth - 50),
      y: Math.random() * (screenHeight - 50),
      vx: Math.random() * 2 - 1,
      vy: Math.random() * 2 - 1,
      color: getRandomColor(),
    }));

    setSpheres(initialSpheres);
  }, []);

  useEffect(() => {
    const animate = () => {
      setSpheres((prevSpheres) =>
        prevSpheres.map((sphere) => {
          let { x, y, vx, vy, color } = sphere;

          x += vx;
          y += vy;

          const screenWidth = window.innerWidth;
          const screenHeight = window.innerHeight;

          if (x <= 0 || x >= screenWidth - 50) {
            vx *= -1;
          }

          if (y <= 0 || y >= screenHeight - 50) {
            vy *= -1;
          }

          return { x, y, vx, vy, color };
        })
      );

      requestRef.current = requestAnimationFrame(animate);
    };

    requestRef.current = requestAnimationFrame(animate);

    return () => {
      if (requestRef.current) {
        cancelAnimationFrame(requestRef.current);
      }
    };
  }, [spheres]);

  return (
    <div className="background" ref={containerRef}>
      {spheres.map((sphere, index) => (
        <div
          key={index}
          style={{
            backgroundColor: sphere.color,
            transform: `translate(${sphere.x}px, ${sphere.y}px)`,
          }}
          className="sphere"
        ></div>
      ))}
    </div>
  );
};

export default Background;
