import { IconType } from "react-icons";
import "./LinkItem.css";

interface LinkItemProps {
  icon: IconType;
  link: string;
}

function LinkItem(props: LinkItemProps) {
  const { icon: Icon, link } = props;

  return (
    <div className="link-item">
      <a href={link} className="a-icon">
        <Icon className="link-icon" />
      </a>
    </div>
  );
}

export default LinkItem;
