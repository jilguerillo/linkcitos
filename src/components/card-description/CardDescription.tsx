import { useTranslation } from "react-i18next";

interface CardDescriptionProps {
  title: string;
  description: string | null;
  url: string | undefined;
}

const CardDescription = (props: CardDescriptionProps) => {
  const { title, description, url } = props;
  const { t } = useTranslation();

  return (
    <section className="card card-side flex items-center shadow-xl m-3  bg-slate-50">
      <div className="card-body">
        <h2 className="card-title text-black">{title}</h2>
        <p className="text-slate-500">{description}</p>
        <div className="card-actions justify-end">
          <a className="btn btn-accent" href={url}>
            {t("button.cardDescription")}
          </a>
        </div>
      </div>
    </section>
  );
};

export default CardDescription;
