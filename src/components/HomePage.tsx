import "./HomePage.css";
import Narvar from "./Narvar";
import Carousel from "./carousel/Carousel";
import Footer from "./Footer";
import Background from "./spheres/Background";
import LanguageSwitcher from "./LanguageSwitcher";

function HomePage() {
  return (
    <body>

      <header>
        <nav>
          <Narvar />
        </nav>
      </header>

      <main>
        <section className="home-page">
          <Background />
          <Carousel></Carousel>
        </section>
      </main>

      <footer>
        <LanguageSwitcher />
        <Footer />
      </footer>
    </body>
  );
}

export default HomePage;
