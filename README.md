<h1 align="center">Linkcitos</h1>

<p align="center">
  Una página para compartir tus enlaces de interés.
</p>

## Tabla de contenidos

- [Demo](#demo)
- [Instalación](#instalación)
- [Uso](#uso)
- [Contribución](#contribución)
- [Licencia](#licencia)

## Demo

Puedes ver el proyecto en https://linkcitos.vercel.app/.

## Instalación

1. Clona el repositorio:

git clone https://gitlab.com/jilguerillo/linkcitos.git

2. Instala las dependencias:

```
cd linkcitos
```
```
npm install
```

## Uso

1. Inicia el servidor de desarrollo:

```
npm start
```

2. Abre la página en tu navegador web en http://localhost:3000.

## Contribución

Si deseas contribuir al proyecto, sigue estos pasos:

1. Haz un fork del repositorio.

2. Crea una rama con tus cambios:

```
git checkout -b my-feature-branch
```

3. Realiza tus cambios y confirma tus cambios:

```
git add .
git commit -m "Descripción de tus cambios"
```

4. Empuja tus cambios a tu repositorio fork:

```
git push origin my-feature-branch
```

5. Crea una solicitud de extracción desde tu repositorio fork a la rama `main` del repositorio original.

6. Espera a que los revisores revisen tu solicitud de extracción y realicen los cambios necesarios.

## Licencia

Este proyecto está licenciado bajo la [GNU GENERAL PUBLIC LICENSE Version 3](LICENSE).```

